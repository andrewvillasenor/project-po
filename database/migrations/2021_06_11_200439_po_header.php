<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PoHeader extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_header', function (Blueprint $table) {
            $table->bigIncrements('po_header_id');
            $table->string('po_header_number', 6);
            $table->string('po_header_company', 255);
            $table->text('po_header_address');
            $table->bigInteger('created_by');
            $table->dateTime('created_date');
            $table->bigInteger('updated_by')->nullable();
            $table->dateTime('updated_date')->nullable();
            $table->smallInteger('activeflag')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('po_header', function (Blueprint $table) {
            //
        });
    }
}