<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PoDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('po_details', function (Blueprint $table) {
            $table->bigIncrements('po_details_id');
            $table->bigInteger('po_details_headerid');
            $table->bigInteger('po_details_qty');
            $table->string('po_details_desc', 255);
            $table->double('po_details_price');
            $table->double('po_details_amount');
            $table->bigInteger('created_by');
            $table->dateTime('created_date');
            $table->bigInteger('updated_by')->nullable();
            $table->dateTime('updated_date')->nullable();
            $table->smallInteger('activeflag')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('po_details', function (Blueprint $table) {
            //
        });
    }
}