<?php

use Illuminate\Support\Facades\Route;

date_default_timezone_set('Asia/Manila');

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home'         , [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/po-datatable' , [App\Http\Controllers\POController::class, 'po_data']);
Route::post('/save_po'     , [App\Http\Controllers\POController::class, 'save_po']);
Route::post('/po_view'     , [App\Http\Controllers\POController::class, 'view_po']);
Route::post('/delete_po'   , [App\Http\Controllers\POController::class, 'delete_po']);