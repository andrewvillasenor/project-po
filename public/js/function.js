var PO = new function(){

    //onload
    this.initial_load = function(){

        PO.load_dataTable();
    }

    this.load_dataTable = function(){

        //po data table
        $('#po_table').attr('data-page-length',10);

        $('#po_table').DataTable({
            // dom: '<"top"f<"toolbar toolbar-02">>t' +
            //     '<"bottom justify-content-between align-items-start"' +
            //     '<"tg-table__pagination"' +
            //     '<"tg-pagination__item"l>' +
            //     '<"tg-pagination__item"i>' +
            //     '<"tg-pagination__item"p>' +
            //     '>' +
            //     '>',
            "responsive" : true,
            "bServerSide": false,
            ajax: {
                url    : '/po-datatable',
                method : 'get',
                'headers': {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data   : function(d) {

                    // return $.extend( {}, d, {
                    //     "result_arr"          : result_arr,
                    // });
                    
                },
                'dataSrc': 'data'
            },
            columns: [
                { title: 'No.', data: 'no', defaultContent: 'ー' },
                { title: 'PO Code', data: 'po_code', defaultContent: 'ー' },
                { title: 'Company', data: 'company', defaultContent: 'ー' },
                { title: 'Address', data: 'address', defaultContent: 'ー' },
                { title: 'Created Date', data: 'created_date', defaultContent: 'ー' },
                { title: 'Action', data: 'action', defaultContent: '' }

            ],
            columnDefs: [
                { "width": "25px", "targets": 0 },
                { "width": "40px", "targets": 1 },
                { "width": "130px", "targets": 2 },
                { "width": "150px", "targets": 3 },
                { "width": "100px", "targets": 4 },
                { "width": "70px", "targets": 5 },

                { targets: [5], orderable: false },
            ],
            drawCallback: function () {

            },
            // rowCallback: function (row, data, index) {

            // },
            initComplete: function() {
                PO.po_func();
            }
        });
    }

    this.po_func = function(){

        //open modal
        $('#po_crud').on('click','#btn_add,.btn_view,.btn_edit',function() {

            var po_id     = $(this).val();
            var po_module = $(this).data('po_module');

            $.ajax({
                type : 'POST',
                url  :   '/po_view',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                    po_id     : po_id,
                    po_module : po_module
                },
                success: function(result){
                    $('#modal #modal-dialog').html(result);

                    if(po_module =='Add')
                    {
                        $('#modal input:text').val('');
                        $('#modal textarea').val('');
                    }

                    $('#modal').modal('show');
                }

            });
            
        });

        //close modal
        $('#modal').on('click', '.close', function() {
            $('#modal').modal('toggle');
        });

        //add new row
        $('#modal').on('click', '#add_row', function(){
            
            var thistable = $(this).closest('table');
            var firsttr   =$('tbody > tr:first', thistable).clone();
            
            $('input:text', firsttr).val('');
            var po_id = $(firsttr).data('po_id',0)
            $(firsttr).attr('data-po_id',0)

            $('tbody',thistable).append(firsttr);
        });

        //compute amount
        $('#modal, #tbl_po_modal').on('change', '.qty,.price', function() {
            var thisrow = $(this).closest('tr');
            var qty     = $('.qty',thisrow).val();
            var price   = $('.price',thisrow).val();
            var amount  = parseInt(qty) * parseFloat(price);
            $('.amount', thisrow).val((amount || ''));
        });

        //save data
        $('#modal').on('click', '#save_data', function() {

            //po header
            var po_id     = $(this).val(),
                company    = $('#company').val(),
                address    = $('#address').val(),
                insert_cnt = 0,
                insert_arr = [],
                update_cnt = 0,
                update_arr = [];

            // validation
            if (company =="")
            {
                toastr.error("Please enter company");
                $('#company').focus();
                return false;
            }

            if (address =="")
            {
                toastr.error("Please enter address");
                $('#address').focus();
                return false;
            }

            //po details
            $('#tbl_po_modal').find('tbody > tr').each(function() {

                var qty        = $(this).find('.qty').val(),
                    desc       = $('.desc', $(this)).val(),
                    price      = $('.price', $(this)).val(),
                    amount     = $(this).find('.amount').val(),
                    po_id      = $(this).data('po_id');
                    
                if((qty == "") || (qty == 0))
                {
                    toastr.error("Please enter quantity");
                    $('.qty', $(this)).focus();
                    return false;
                }
                
                if(desc == "")
                {
                    toastr.error("Please enter description");
                    $('.desc', $(this)).focus();
                    return false;
                }

                if((price == "") || (price == 0))
                {
                    toastr.error("Please enter unit price");
                    $('.price', $(this)).focus();
                    return false;
                }

                if((amount == "") || (amount == 0))
                {
                    toastr.error("Invalid amount");
                    $('.amount', $(this)).focus();
                    return false;
                }

                if (po_id == 0)
                {
                    insert_arr[insert_cnt] = {qty: qty, desc: desc, price: price, amount: amount};

                    insert_cnt++;
                }
                else
                {
                    update_arr[update_cnt] = {po_id: po_id, qty: qty, desc: desc, price: price, amount: amount};
                    
                    update_cnt++;
                }
                
            });

            if((insert_arr != "") || (update_arr != ""))
            {
                $.ajax({
                    type: 'POST',
                    url:    '/save_po',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        po_id     : po_id,
                        company   : company,
                        address   : address,
                        insert_arr: insert_arr,
                        update_arr: update_arr
                    },
                    success: function(result)
                    {
                        if(result == "Company existing")
                        {
                            toastr.warning("Company existing");
                            $('#company').focus();
                        }
                        else
                        {
                            if(result)
                            {
                                toastr.success("Record saved");
                                $('#po_table').DataTable().ajax.reload();
                                $('#modal').modal('toggle');
                            }
                            else
                            {
                                toastr.errror("Record not saved");
                            }
                        }
                        
                    },
                    error: function(result)
                    {
                        toastr.errror("Error");
                    }
                });
            }

        });

        //delete po
        $('#po_crud').on('click', '.btn_delete', function() {

            var po_id = $(this).val();
            
            $.ajax({
                type    :  'POST',
                url     :  '/delete_po',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data    : {
                    po_id : po_id
                },
                success: function(result) {

                    if(result > 0)
                    {
                        toastr.success("Record deleted");
                        $('#po_table').DataTable().ajax.reload();
                    }
                    else
                    {
                        toastr.errror("Record not deleted");
                    }
                },
                error: function(result) {
                    toastr.errror("Error");
                }
            });
        });
    }
}

PO.initial_load();