<table id="po_table" class="table table-striped table-bordered table-hover table-condesed">
    <thead class="thead-dark">
        <tr>
            <th class="sorting" data-sorting_type="asc" data-col_name="code">PO Code <span id="code_icon"></span></th>
            <th class="sorting" data-sorting_type="asc" data-col_name="company">Company <span id="company_icon"></span></th>
            <th>Address</th>
            <th>Created Date</th>
            <th>Created Date</th>
        </tr>
    </thead>
    <tbody>
    @if (!empty($po_list ))
        @foreach($po_list as $index => $row)
        <tr>
            <td>{{$row->po_header_number}}</th>
            <td>{{$row->po_header_company}}</td>
            <td>{{$row->po_header_address}}</td>
            <td>{{$row->created_date}}</td>
            <td>
                <center>
                    <button type="button" class="btn btn-primary w-25 p-1 btn_view" value="{{$row->po_header_id}}" data-po_module="View">View</button>
                    <button type="button" class="btn btn-warning w-25 p-1 btn_edit" value="{{$row->po_header_id}}" data-po_module="Edit">Edit</button>
                    <button type="button" class="btn btn-danger w-25 p-1 btn_delete" value="{{$row->po_header_id}}" data-po_module="Delete">Delete</button>
                </center>  
            </td>
        </tr>
        @endforeach
    @else
        <tr>
            <td colspan="4">No records found.</td>
        </tr>
    @endif
    </tbody>
</table>