@extends('layouts.app')

@section('content')
<div id="po_crud" class="container">
    <div class="table-wrapper box--shadow">
        <div class="table-title">
            <div class="row">
                <div class="col-sm-6">
                    <h2>Manage <b>PO</b></h2>
                </div>
                <div class="col-sm-6">
                <button id="btn_add" type="button" class="btn btn-success mb-2 float-right" value="0" data-po_module="Add"> Add PO</button>						
                </div>
            </div>
        </div>

        <table id="po_table" class="table table-striped table-bordered nowrap" style="width:100%">
        </table>
    </div>
</div>
@endsection
