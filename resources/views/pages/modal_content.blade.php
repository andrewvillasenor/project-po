<div class="modal-content">
    <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">{{ !empty($po_module)? $po_module.' PO' : '' }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div id="modal_content" class="container-fluid">
            <div class="row">
            <div class="col-lg-3">Company</div>
            <div class="col-lg-9"><input type="text" id="company" class="form-control" value="{{ !empty($po_header_com)? $po_header_com : '' }}" {{ ($po_module == 'View')? 'disabled' : ''  }}></div>
            </div>
            <div class="row">
            <div class="col-lg-3">Address</div>
            <div class="col-lg-9"><textarea id="address" class="form-control" {{ ($po_module =='View')? 'disabled' : '' }}>{{ !empty($po_header_adr)? $po_header_adr : '' }}</textarea></div>
            </div>
            <div class="row mt-5">
                <table id="tbl_po_modal" class="table table-bordered table-condensed table-striped table-hover text-center">
                <thead>
                    <tr>
                    <th scope="col">QTY</th>
                    <th scope="col">Description</th>
                    <th scope="col">Unit Price</th>
                    <th scope="col">Amount</th>
                    </tr>
                </thead>
                <tbody>
                @if(!empty($po_details))
                    @foreach ($po_details as $row)
                        <tr data-po_id="{{$row->po_details_id}}">
                            <td><input type="text" class="form-control qty" value="{{$row->po_details_qty}}" {{ ($po_module == 'View')? 'disabled' : '' }}></td>
                            <td><input type="text" class="form-control desc" value="{{$row->po_details_desc}}" {{ ($po_module == 'View')? 'disabled' : '' }}></td>
                            <td><input type="text" class="form-control price" value="{{$row->po_details_price}}" {{ ($po_module == 'View')? 'disabled' : '' }}></td>
                            <td><input type="text" class="form-control amount" value="{{$row->po_details_amount}}" readonly></td>
                        </tr>
                    @endforeach
                @else
                    <tr data-po_id="0">
                        <td><input type="text" class="form-control qty" placeholder="Quantity"></td>
                        <td><input type="text" class="form-control desc" placeholder="Description"></td>
                        <td><input type="text" class="form-control price" placeholder="Price"></td>
                        <td><input type="text" class="form-control amount" placeholder="Amount" readonly></td>
                    </tr>
                @endif
                </tbody>
                <tfoot>
                    <tr>
                    <td colspan="4">
                        <button id="add_row" type="button" class="btn btn-primary" {{ ($po_module == 'View')? 'disabled' : '' }}>Add Row</button>
                    </td>
                    </tr>
                </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    <button id="save_data" type="button" class="btn btn-primary" value="{{$po_id}}" {{ ($po_module == 'View')? 'disabled' : '' }}>Save</button>
    </div>
</div>