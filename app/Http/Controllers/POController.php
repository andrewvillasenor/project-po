<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\POModel;

class POController extends Controller
{
    public function po_data(Request $request)
    {
        $i_limit        = $request->input('length');
        $i_page         = $request->input('start');
        $search_value   = $request->input('search.value');
        $columns        = array(
            0   => 'no',
            1   => 'po_code',
            2   => 'company',
            3   => 'address',
            4   => 'created_date',
            5   => 'action'
        );

        $a_data = POModel::po_list($i_limit,$i_page);

        $new_array   = [];
        if(!empty($a_data))
        {
            foreach($a_data as $key => $data)
            {
                foreach($columns as $index => $value)
                {

                    if($value == 'no'){
                        $new_array[$key][$value]    = $key+$i_page+1;
                    }
                    else if($value == 'action'){
                        $new_array[$key][$value]    = '
                            <button type="button" class="btn btn-primary w-30 p-1 btn_view" value="'.$data['id'].'" data-po_module="View">View</button>
                            <button type="button" class="btn btn-warning w-30 p-1 btn_edit" value="'.$data['id'].'" data-po_module="Edit">Edit</button>
                            <button type="button" class="btn btn-danger w-30 p-1 btn_delete" value="'.$data['id'].'" data-po_module="Delete">Delete</button>
                        ';
                    }
                    else if (empty($data[$value])) { // FOR COLUMNS THAT HAS NO DATA YET

                        $new_array[$key][$value]    = '-';
                    }
                    else {
                        $new_array[$key][$value]    = $data[$value];
                    }
                }
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => count($new_array),
            "recordsFiltered" => count($new_array),
            "data"            => $new_array
        );

        echo json_encode($json_data);
    }

    public function save_po (Request $request)
    {
        $po_id      = $request->input('po_id');
        $company    = $request->input('company');
        $address    = $request->input('address');
        $insert_arr = $request->input('insert_arr');
        $update_arr = $request->input('update_arr');

        if(!empty($insert_arr))
        {
            return $this->insert_po($po_id, $company, $address, $insert_arr);
        }

        if(!empty($update_arr))
        {
            return $this->update_po($po_id, $company, $address, $update_arr);
        }
 
    }

    public function insert_po($po_id = 0, $company = '', $address = '', $insert_arr = []) 
    {

        if($po_id == 0)
        {
            $return_array = POModel::insert_po_header($company, $address);
            
            if($return_array == 'Company existing')
            {
                return 'Company existing';
            }

            foreach($return_array as $id)
            {
                $po_id = $id->po_header_id;
            }
        }

        $insert_details_arr = [];
        foreach($insert_arr as $index => $row)
        {
            $insert_details_arr[$index] = array(
                'po_details_headerid'   =>  $po_id,
                'po_details_qty'        =>  $row['qty'],
                'po_details_desc'       =>  $row['desc'],
                'po_details_price'      =>  $row['price'],
                'po_details_amount'     =>  $row['amount'],
                'created_by'            =>  1,
                'created_date'          =>  date('Y-m-d H:i:s'),
            );
        }
        
        $response = POModel::insert_po_details($insert_details_arr);

        return json_encode($response);
        
    }

    public function view_po(Request $request)
    {
        $po_id         = $request->input('po_id');
        $po_module     = $request->input('po_module');
        $po_header     = POModel::po_header($po_id);
        $po_details    = POModel::po_details($po_id);
        $po_header_com = '';
        $po_header_adr = '';

        foreach($po_header as $row)
        {
            $po_header_com = $row->po_header_company;
            $po_header_adr = $row->po_header_address;
        }

        $po_data    = [
            'po_id'         => $po_id,
            'po_module'     => $po_module,
            'po_header_com' => $po_header_com,
            'po_header_adr' => $po_header_adr,
            'po_details'    => $po_details
        ];

        return view('pages.modal_content', $po_data);
    }

    public function update_po($po_id = 0, $company = '', $address = '', $update_arr = [])
    {
        $int_header = POModel::update_po_header($po_id, $company, $address);

        if($int_header == 'Company existing')
        {
            return 'Company existing';
        }
        else
        {
            if($int_header > 0)
            {
                $int_details = POModel::update_po_details($update_arr);
            
                return json_encode($int_details);
            }
        }
    }

    public function delete_po(Request $request) {

        $po_id = $request->input('po_id');
        
        $int_header = POModel::delete_po_header($po_id);

        if($int_header > 0)
        {
            $int_details = POModel::delete_po_details($po_id);

            return json_encode($int_details);
        }

    }
}
