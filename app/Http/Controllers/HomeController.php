<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $limit  = 5;
        $search = '';

        // $po_list = POModel::po_list($limit, $search);
        
        $po_list = DB::table('po_header')->where('activeflag', 1)->orderBy('po_header_id')->paginate(10);
        // $po_list = POModel::all();
        $po_list->setPath('/home');

        // print_r($this->url->to('/'));
        // die();
        return view('pages.home',['po_list' => $po_list]);
    }
}
