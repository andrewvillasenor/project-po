<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class POModel extends Model
{
    use HasFactory;

    public static function po_list($i_limit = '', $i_page = '')
   {
        $sql = "
            SELECT 
                po_header_id AS id, 
                po_header_number AS po_code, 
                po_header_company AS company, 
                po_header_address As address, 
                created_date
            FROM po_header
            WHERE activeflag = 1
            ORDER BY po_header_number
        ";

        if(!empty($i_limit) && !empty($i_limit))
        {
            $sql .= "
                LIMIT
                    ".$i_limit."
                OFFSET
                    ".$i_page."
            ";
        }

        $result = DB::select($sql);
    
        return !empty($result) ? json_decode(json_encode($result),true) : [];
   }
    
    public static function insert_po_header ($company = '', $address = '')
    {
        // validate company if existing
        $result = DB::table('po_header')
        ->where('po_header_company', '=', $company)
                ->count();

        if($result > 0)
        {
                return "Company existing";
        }
        else
        {
                //get maximum po number
                $po_max_num = DB::table('po_header')->max('po_header_number');
                $po_num     = str_pad(1,6,0, STR_PAD_LEFT);

                if(!empty($po_max_num))
                {
                        $po_num = str_pad($po_max_num + 1,6,0, STR_PAD_LEFT);
                }

                //data to insert
                $data = array(
                        'po_header_number'      =>    $po_num,
                        'po_header_company'     =>    $company,
                        'po_header_address'     =>    $address,
                        'created_by'            =>    1,
                        'created_date'          =>    date('Y-m-d H:i:s')
                );

                //insert data and get the id
                // $last_id = DB::table('po_header')->insertGetId($data);

                $result_cnt = DB::table('po_header')->insert($data);

                $sql = "
                        SELECT po_header_id
                        FROM   po_header
                        WHERE  po_header_number = ?
                ";

                 $last_id = DB::select($sql, array($po_num));

                return $last_id;
        }
    }

   public static  function insert_po_details($insert_details_arr = []) 
   {
        $output =  DB::table('po_details')->insert($insert_details_arr);

        return $output;
   }


   public static function po_header($po_id = 0)
   {
        $sql = "
                SELECT po_header_company, po_header_address
                FROM   po_header
                WHERE  po_header_id = ?
        ";

        $po_head = DB::select($sql, array($po_id));

        return $po_head;
   }

   public static function po_details($po_id = 0)
   {
        $sql = "
                SELECT po_details_id, po_details_qty, po_details_desc, po_details_price, po_details_amount
                FROM   po_details
                WHERE  po_details_headerid = ?
        ";

        $po_det = DB::select($sql, array($po_id));

        return $po_det;
   }

   public static function update_po_header($po_id = 0, $company = '', $address = '')
   {

        $result = DB::table('po_header')
        ->where('po_header_company', '=', $company)
                ->count();

        if($result > 0)
        {
                return "Company existing";
        }
        else
        {
                $sql = "
                        UPDATE po_header
                        SET po_header_company = ?, po_header_address = ?, updated_by = ?, updated_date = ?
                        WHERE po_header_id =?
                ";

                $result_header  = DB::update($sql, array($company, $address, 1, date('Y-m-d H:i:s'), $po_id));

                return $result_header;    
        }
   }

   public static function update_po_details($update_arr = [])
   {
        $sql = "
                UPDATE po_details
                SET po_details_qty = ?, po_details_desc = ?, po_details_price = ?, po_details_amount = ?, updated_by = ?, updated_date = ?
                WHERE po_details_id =?
        ";

        foreach($update_arr as $row)
        {
                $result_details = DB::update($sql, array($row['qty'], $row['desc'], $row['price'], $row['amount'], 1, date('Y-m-d H:i:s'), $row['po_id']));
        }

        return $result_details;
   }

   public static function delete_po_header($po_id = 0)
   {
        $sql = "
                DELETE FROM po_header WHERE po_header_id =?
        ";

        $result_header = DB::delete($sql, array($po_id));

        return $result_header;
   }

   public static function delete_po_details($po_id = 0)
   {
        $sql = "
                DELETE FROM po_details WHERE po_details_headerid = ?
        ";

        $result_details = DB::delete($sql, array($po_id));

        return $result_details;
   }
}
